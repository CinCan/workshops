# Memory analysis, part 1 - finding malicious artefact

## Sample

There is zipped memory dump (79MB as compressed, 269MB extracted) named as `memorydump.zip`.

You can get it from [here.](../../Disobey-2020/MalwareMemorydumpTask/memorydump.zip)

There is also another file `mystery.enc` which is encrypted and its purpose might come up later. It is stored in [here.](../../Disobey-2020/MalwareMemorydumpTask/mystery.enc)

If you have cloned this repository, those files are in Disobey-2020 folder under memory dump task, with similar names.

**!!! Warning !!! : As decrypted, it should not be executed on Windows machine, it might get bit stuck...** 

## Background

Digital forensics can contain many different kind of steps and challenges in order to collect some required knowledge.

This case contains memory dump analysis with further analysis of acquired artifact: malicious Word document. Document analysis is done in the second part, and goes even beyond of the content of Word document.

In practice, we are attempting to use many different tools for identifying potentially malicious file!


## Little background about the dump file

Officials caught known cyber criminal who was anonymously tipped to plan to be preparing cyber attack. Crashing computers!

During takeover they got memory dump from his machine (so called "cold boot attack"), before it was shut down.
Apparently he was just *reading some emails* when got caught, before locking his machine.

Your task is to find out, if there were any truth behind the anonymous tip!

### Set of tools

[cincan command](https://gitlab.com/CinCan/cincan-command) can be used to run almost any CLI based Docker containers without using volumes and we encouraging to try it out. We expect, that this is installed and fully working at this point.

We have Dockerized set of commonly used tools, which can useful here, but you are free to use anything you like. 

In the first part, the some useful tools:

* cincan/volatility
* cincan/binwalk (maybe)

Many regular command line tools such as `grep`,  `base64`, `jq`, `file`, and can be generally helpful.

## Getting started
 
One of the most used tool for memory forensics is Volatility.

Some tutorials for using the Volatility (The latter for the plugins in this context is the most useful):

  * [Volatility general usage](https://github.com/volatilityfoundation/volatility/wiki/Volatility-Usage)

  * [Command reference for plugins](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference)

In general, the -h or --help option of each tool could be useful.


We have made Docker container of Volatility, tagged as `cincan/volatility`. You can use it with cincan-command but feel free to use it in other ways as well.


Some useful plugins of volatility to start with:

> imageinfo : plugin for detecting possible OS version of the dump 

> pslist and pstree : listing open processes

> dumpfiles: extract cache files related to process

> memdump: extract memory content belonging to process

Example: 
```
cincan run cincan/volatility -f dump.raw imageinfo
```

This can estimate the profile of the memory dump. It can be used in further commands!

### Initial idea

There was mention about emails, could we somehow acquire them from the memory dump? 

  * Are there some interesting processes? 
  * Can we access to the content of the processes?

There are totally two interesting processes, which are not directly belonging to underlying operating system.


### Extracting files

Once identifying the interesting processes, [dumpfiles plugin](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference#dumpfiles) of Volatility could do a lot in this case. It has possibility extract all cached files related to process or regex pattern. 

We could for example find all files which match for word `dog` with following command. 

```
 cincan run --mkdir output cincan/volatility --profile=<PROFILE> -f dump.raw dumpfiles -S summary.txt -r "dog" -D output
```

> Use `--mkdir` or `-d` argument to prevent existing result output files to be re-uploaded into container, if running same command again. It also creates this folder locally and inside container. Magic is limited for some point.


However, there are other ways than regex for finding files. `Summary.txt` file **might be very important** in mapping of filesystem filename and extracted filename. It gives clues about the purposes and locations of the files. Note that it is in `.json` format and for example tool `jq` can be useful for linting it. Without regex, *all process related* files can be acquired.

Another way to proceed is to use `memdump` plugin.
With memdump plugin, we could extract memory area belonging process. What if try to use `cincan/binwalk` here for the output?


Without spoiling too much for the fun, email might give a clue for next step. How possible email clients are actually storing messages? What is the technology in this case?

There was another interesting process what we can guess based on previous information, and because of that, we can acquire one interesting file which is cached in to the memory.

The information from email with knowledge of purpose of other process and content of it, should be enough for finding malicious artefact!


## Memory analysis, part 2  - analysing Word document

The second part contains more tools with more variety. Previously acquired Word document doing something under the hood.

It is found compressed as `document.zip`, with password `infected`, in case first part is not completed.

Following tools could be helpful with analysis>

* cincan/oledump - document analysis
* cincan/oletools - document analysis
* cincan/ilspy - .NET Assembly decompiler
* cincan/ghidra-decompiler - headless Ghidra decompiler for reverse engineering
* cincan/radare2 - general disassembler/reverse-engineering

### The document


Now that we have got hands into the document, we are very eager to know what it actually does!

Some example tools related analysing OLE - based files (Like Word .doc files):

* [cincan/oletools](https://gitlab.com/CinCan/tools/-/tree/master/oletools)
* [cincan/oledump](https://gitlab.com/CinCan/tools/-/tree/master/oledump)

Based on the analysis:

  * Is it containing macros?
  * What they are doing?


### Static analysis

There is actually binary file inside that Word document as well. Somehow it has been stored into it. What can you tell sizes about different sections based on oletools/oledump?
Can you find and extract binary?

Once the binary has been rewritten to disk, for further analysis, some tools can be useful. (Only one actually produces useful output for quick analysis in this case!)

* [cincan/ilspy](https://gitlab.com/CinCan/tools/-/tree/master/ilspy)
* [cincan/ghidra-decompiler](https://gitlab.com/CinCan/tools/-/tree/master/ghidra-decompiler)
* [cincan/radare2](https://gitlab.com/CinCan/tools/-/tree/master/radare2)

For example tool `file` might give insight about what other tool could be useful in this case.

Ilspy is capable for decompiling some .NET Assemblies.

ghidra-decompiler can decompile whole binary (as long as instructions supported) to STDOUT by default as pseudo-C code, which can be redirected to file. 

Radare2 contains ghidra decompiler plugin as well, but as controlled CLI based disassembler, it can offer much more! However, it requires bit more experience to know commands.

To use radare2 with `cincan command`, 

(-i, and -t flags, as interactive and pseudo-tty to make interactive session with radare2)

```
cincan run -it cincan/radare2 r2 /bin/ls # /bin/ls as input file
cincan/radare2: <= /usr/bin/ls
 -- Mind that the 'g' in radare is silent
[0x00005b10]> aaa
[x] Analyze all flags starting with sym. and entry0 (aa)
[x] Analyze function calls (aac)
[x] Analyze len bytes of instructions for references (aar)
[x] Check for objc references
[x] Check for vtables
[x] Type matching analysis for all functions (aaft)
[x] Propagate noreturn information
[x] Use -AA or aaaa to perform additional experimental analysis.
[0x00005b10]> afl  # To list all methods of binary
[0x00005b10]> pdf @main # to see assembly instructions of main
[0x00005b10]> pdg @main # to decompile with Ghidra plugin the main function into pseudo-C code
```

## One file more

Based on analysis of previous of file, we can detected some features of the malware:

  * How it is attempting to persist in system?
  * Is it actually the binary doing all the "malicious" things? Seems like it is more like a dropper getting even more files.
  * It is fetching one file more over the internet. We happen to have this file already in this repository. Can we open and analyze it?

## And finally...

  * How is this last file leading for potential crash of computer?

