# Phishing email task

You get some emails forwarded to you by a concerned citizen about potential
phishing campaigns, figure out if they are malicious or not.

## Difficulty level

Beginner [ ]

Intermediate [x]

Professional [ ]

## Samples

*!!! Warning !!! : the sample mails contain real malware, please be extra
careful with the handling of the files*

There are multiple samples containing the raw email messages forwarded as
`.eml` files in the `samples.zip`. Password is **`infected`**.

The sample `xppp.exe` is an executable fetched from a remote server, which does
not exists anymore, so it is included in the samples as is. If you want to play
by the rules,  start working on it when you have found the URL to the remote
server.

As the `.eml` files are plain text, you *can* open them and investigate them
with your favourite text editor. The content or body of the mail is usually
`base64` encoded, though, so it is better to use a parser to extract
information.

The tool `eml_parser` can extract data out of the format, such as links,
email addresses, files, etc...

Some goals for this execise:

1. Determine which of the emails are malicious and what to investigate further

2. Most phishing campaigns rely on visual deception. Try to capture the visual
   look of the emails to determine how shady the content is
   (hint: headless-thunderbird)

2. If malicious, figure out what the email is trying to achieve. Credential
   harvesting, [CEO fraud](https://www.sans.org/security-awareness-training/ouch-newsletter/2016/ceo-fraud)
   or just dropping some malware on the user.

3. Investigate the infrastructure of the sender and possible steps to stop the
   possible phishing campaigns, like domains and hosting companies

4. Investigate the artifacts for IoCs (Indicators of Compromise), like URLs,
   files, commands, scripts etc.

5. Can you find the samples from somewhere else on the internet? Can you identify a
   "known" strain of malware?

## Tools useful for this task

* cincan/scrape-website
* cincan/jq
* cincan/headless-thunderbird
* cincan/oledump
* cincan/ilspy
* cincan/vipermonkey
* cincan/de4dot

## Getting started

### Analyze web content

The tool `scrape-website` can be used to produce a `har` (HTTP Archive) file. It
is a JSON format to store requests made in the Chromium browser.
To get all urls from a HTTP Archive file (HAR file), you can use the ever useful
`jq` to wrangle useful data out of JSON

```shell
cincan run cincan/jq '.log.entries[] | .request.url ' file.har | awk -F[/:] '{print $4}' | sort | uniq
```

### Analyze Microsoft Office documents

When you come up against a Office document (Word, Excel), you can analyze it
with tools such as:

* [cincan/oletools](https://gitlab.com/CinCan/tools/-/tree/master/oletools)
* [cincan/oledump](https://gitlab.com/CinCan/tools/-/tree/master/oledump)
* [cincan/vipermonkey](https://gitlab.com/CinCan/tools/-/tree/vipermonkey/vipermonkey)

Things to look for are Macros (VBA code). Macros usually drop a binary on to
disk from a C2 (Command & Control) server to have a custom payload for each
document or user.

ViperMonkey is very slow at emulating large VBA scripts, so you need to be
patient.

### Analyze .NET executables

Usually .NET malware is obfuscated, but we can reverse the obfuscation with
a .NET deobfuscator
[`de4dot`](https://gitlab.com/CinCan/tools/-/tree/de4dot/de4dot).

You can continue to decompile the executable with
[`ilspy`](https://gitlab.com/CinCan/tools/-/tree/master/ilspy). Take in to
consideration that malware can load additional payloads from resources, so just
decompiling the code might not reveal everything.

### Powershell

If you find some Powershell and you want to deobfuscate the code without having
a Windows installation at hand, you can open a Powershell shell with the
`docker` command quite easily with the command:
```
docker run --rm -it mcr.microsoft.com/powershell
```
 (`cincan-command` is not working currently for the Powershell image from
Microsoft)

