# Workshop at Haaga-Helia Pentest course

Welcome to this DFIR (digital forensics & incident response) workshop!

## Environment instructions

This workshop utilizes a physical or virtual machine as the environment for using the analysis tools.
You should be running Ubuntu 18.04 or later. Apart from the base Ubuntu system you need to have *Docker* installed (easiest: install the Ubuntu "docker.io" package).

* Ubuntu 18.04
* Python 3.6 or newer
* Cincan-command from pip 'pip install cincan-command'
* Docker 18 or newer
