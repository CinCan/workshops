# Phishing email task

You get some emails forwarded to you by a concerned citizen about potential phishing campaigns, figure out if they are malicious or not.

## Difficulty level

Beginner [ ]

Intermediate [x]

Professional [ ]

## Samples

There are multiple samples containing the raw email messages forwarded as `.eml` files. Some headers unfortunately have been lost during the forward.

You can open the `.eml` files with your favourite text editor. As they are plain text, you can read the headers of the received mail quite easily.
You will need to decode base64 encoded data from the mail body. You can use the tool `eml-parser` in the cincan repository.

Some goals for this execise:

1. Determine which of the emails are malicious and what to investigate further

2. Most phishing campaigns rely on visual deception. Try to capture the visual look of the emails to determine how shady the content is (hint: thunderbird)

2. If malicious, figure out what the email is trying to achieve. Credential harvesting, [CEO fraud](https://www.sans.org/security-awareness-training/ouch-newsletter/2016/ceo-fraud) or just dropping some malware on the user

3. Investigate the infrastructure of the sender and possible steps to stop the possible campaigns (hint: scrape-website)

### We need the headers!

Some samples got forwarded to us from somebody without headers. This is because email clients only save some headers from the message when saving on to disk.
You will be missing the `X-*`, authorization and signature headers. Checking these usually easily shows if the email was spoofed or not.

If you are interested in some information about email headers and how they are spoofed, read on!

### Spoofing different headers

#### Return-Path

`Return-Path` header might show the real address of the sender, not the spoofed one. This header is verified by [SPF](https://en.wikipedia.org/wiki/Sender_Policy_Framework).
If we cannot check the `Return-Path`, we can check whether the domain is associated with any email servers with `dig`:

```
dig +noall +answer pystyyvetaa[.]com mx
```

This yields no mail server (MX) for the domain, so there can't be any SPF records. In conclusion, the sender address was spoofed.


#### Spoofing the name

The name in `From` field can be easily spoofed, and it is not verified by anything:

```
Return-Path: Bad Guy <evil@pystyyvetaa[].com>
...
From: The CEO <evil@pystyyvetaa[.]com>
```

We can check whether the email address in the `From` field is correct by checking the `Received:` header:
```
Received: from [mail[.]pystyyvetaa[.]com] (mail[.]pystyyvetaa[.]com [x.x.x.x])

```

#### Check SPF records

The command:

```
dig +noall +answer disobey[.]fi txt
```

should yield an answer something like this:

```
v=spf1 ip4:x.x.x.x, ip4:x.x.x.x +a +mx a:example[.]com -all
```

or something similar. The originating STMP server should match with one of these. You can check other domains for these records.

