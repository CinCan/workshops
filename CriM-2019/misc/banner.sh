#!/bin/sh

is_alive="$(curl -Ias --connect-timeout 3 https://gitlab.com/CinCan/workshops/raw/master/CriM-2019/misc/short_intro.txt | head -n 1 | cut -d ' ' -f2)"

if [ "$is_alive" = "200" ]; then
        curl -s https://gitlab.com/CinCan/workshops/raw/master/CriM-2019/misc/short_intro.txt > cat init/intro.txt
        cat init/intro.txt
   else
        figlet -w 200 "CriM 2019 - CinCan Workshop"
fi


