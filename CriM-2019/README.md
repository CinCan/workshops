# CriM 2019 - CinCan Workshop

Welcome to CinCan Workshop!

See instructions for malware analysis case in [here.](malwareAnalysis.md)

There are instruction for using the analysis environment (virtual machine) below.

## Virtual machine instructions

This workshop utilizes single virtual machine as our desktop environment for using the analysis tools.
Virtual machine is based on Ubuntu 18.04.3 LTS.

Credentials for virtual machine are:

Username: **crim**  
Password: **crim**


This virtual machine is located in the University network drive, and can accessed as following:

 * Open Windows Command Line 

 * Mount the network drive

```
net use z: "\\kaappi\Virtuaalikoneet$"
```

Files should be available now at `Z:\\kaappi\Virtuaalikoneet$\VMware\CriM\`  
The *whole* Ubuntu machine can be copied from there to local machine, for example into the C:\\Temp folder. It can be started by double clicking `Ubuntu64-bit-CriM.vmx`

**However, here is shortcut to run virtual machine directly from the network drive.**

Just run following scrip in the Windows Command Line:

```
(if exist "Z:" (echo "Drive already mounted") else (net use z: "\\kaappi\Virtuaalikoneet$")) && (if exist "C:\Temp\Ubuntu64-bit-CriM" (rd /s /q "C:\Temp\Ubuntu64-bit-CriM" && mkdir "C:\Temp\Ubuntu64-bit-CriM") else (mkdir "C:\Temp\Ubuntu64-bit-CriM")) && copy "Z:\VMware\CriM\Workshop1\Ubuntu64-bit-CriM\Ubuntu_copy_and_run_me.vmx" "C:\Temp\Ubuntu64-bit-CriM\Ubuntu_copy_and_run_me.vmx" && start "" "C:\Temp\Ubuntu64-bit-CriM\Ubuntu_copy_and_run_me.vmx"
```

This copies file `Ubuntu_copy_and_run_me.vmx` to correctly named place (C:\Temp\Ubuntu64-bit-CriM), and starts VM as using images from network drive.

NOTE: In general, C:\\Temp folder is shared among all users in the machine, and not so recommended for general use from the security perspective. However, usability is enemy in this case...

### Virtual machine keyboard layout

Virtual machine keyboard layout is set to be 'en/US' by default (fi-FI as secondary), but you can add suitable one for yourself by going into:
 * Settings -> 
 * Region & Language ->
 * Add input source + -> 
 * Press three dots -> 
 * select other ->
 * [search for your choice] ->
 * move it up as first language

Make sure to close any windows, and then, finally, by pressing [WIN key] + SPACE, input layout language is changed. (Or click language in top-right corner to change.)



