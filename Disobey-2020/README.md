# Workshop at Disobey 2020

Welcome to this DFIR (digital forensics & incident response) workshop!

## Environment instructions

This workshop utilizes a physical or virtual machine as the environment for using the analysis tools.

You are *recommended* to be running Ubuntu 18.04 or later. Apart from the base Ubuntu system you need to have *Docker* installed (easiest: install the Ubuntu "docker.io" package) and Python 3.7+.

Arch Linux based systems should work flawlessly as well, and probably many other distributions as well, however they are not tested.

In MacOS works mostly, but there are some distractions - best user experience not promised.

Or you can use your own Laptop
* Ubuntu 18.04 (However, not mandatory, other Linux distribution should work as well)
* Python 3.7 or newer
* Cincan-command from pip 'pip3 install cincan-command' (use [e.g. venv](https://docs.python.org/3/tutorial/venv.html) , or you can install it with --user flag. This expects that .local bins are set to PATH)
* Docker 18 or newer (the version you get from `apt-get install docker.io` in Ubuntu 18 is ok)
