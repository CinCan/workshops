# Log analysis task


## Difficulty level

Beginner [ ]

Intermediate [ ]

Professional [X]


## Sample
The log file of web server can be found from file `anonlog.zip` from this repository. It has **6264487** lines of text, which **size of 655 megabytes.**


# Log file analysis

Incident response might start many times from pure log analysis. You have a pile of log files, which might contain million lines of text, and all of these need to be analysed to find evidence, for what is going on or has already happened. Maybe there is some malicious activity causing some harm, and we would like to track it, or even more importantly, to prevent it happening in future.

Sometimes these log files are formatted in totally different way than they were last time, and nothing makes sense. Your loyal all-time log-analysis-tool-friend is not working, because it is incompatible with this new format and no automatic conversion tool was found.

Are we helpless? No! We can grab our favorite *nix system and we can take very good use of basic command utils such as `awk`,  `grep`, `uniq` or `cut` and many others (on *nix side),  and let's see what we can do!

## Case: investigating DDoS attack from pure log file

> Log file is based on real-life incident, however the story is a bit transcribed here.

There has been a serious incident. CinCan's website for after party sauna event has been under DDoS attack, and it simply can't handle it. The whole site is jammed and nobody can reserve anything. The whole event is damned! Seems like entry is open for everyone. But can we possibly figure out, who was behind of attack? In this case, it can be well reasoned.

The log file of web server can be found from file `anonlog.zip`. It has **6264487** lines of text, which **size of 655 megabytes.**

The structure of log file is quite typical in this case (W3C Logging), and fields are as following, when looking head of the file:

```
#Software: Microsoft Internet Information Services 7.5
#Version: 1.0
#Date: 1970-01-01 00:00:00
#Fields: date time cs-method cs-uri-stem cs-uri-query cs-username c-ip cs-version cs(User-Agent) cs(Referer) cs-host sc-status sc-bytes time-taken
```

But how to get started? File is rather massive and might lead into crashing or PC when opened in regular text editor.

In scenario like this, we might want to figure out some simple information at first: 

 * When was the peak of DDoS Attack? (=timeline?) 
 * When it actually started? Was it potentially exploiting some weakness of the system?

When we figure these things out, and look requests more precisely, *we can give a valid guess what IP address belongs to controller of attack!*

### Using *nix command line tools

Tool named `less` might be handy for general observation of file - it is not opening the whole file at once.

`awk` is general tool for processing, well it is *programming language*. How about to print third and fourth column, seperated by space? Using `tail` and `head` to show only lines 5-10
```
awk '{print$3,$4;}' anonlog | head -n 10 | tail -n +5
```
Or awk alone:

```
awk 'NR>=5&&NR<=10 {print$3,$4;}' anonlog
```


`cut` can be useful as well when handling columns of file
At first, we might be interested about about *time and date* columns. 

`uniq` tool can extract many unique properties! Can we use it to get some timeline properties from log file?

`sort` can be used for sorting, surprisingly. `grep` can be used to match strings from file, in addition of search property of `less` tool. 

These are just couple examples, use anything you want!
