## FIRMWARE ANALYSIS

## Difficulty level

Beginner [ ]

Intermediate [x]

Professional [ ]


## Sample

Download any firmware that you're interested in and try out our tools!

## Insctructions

Recommended tools: cincan tools, binwalk, ghidra, radare, FLOSS, strings.
Introduction to the tools:

https://cincan.io/blog/2019_12_14_ghidra/

https://cincan.io/blog/2019_12_15_strings/

https://cincan.io/blog/2019_12_18_binwalk/

https://cincan.io/blog/2019_12_20_radare2/

https://embeddedbits.org/reverse-engineering-router-firmware-with-binwalk/

https://cincan.io/blog/2019_12_21_floss/

## Example

Reversing TP-LINK router WR841 file system from the firmware:

Step 1: Download the binary: https://static.tp-link.com/2019/201911/20191120/TL-WR841N(US)_V14_191119.zip

```
unzip TL-WR841N\(US\)_V14_191119.zip 
cp TL-WR841N\(US\)_V14_191119/TL-WR841Nv14_US_0.9.1_4.16_up_boot\[191119-rel54857\].bin firmware.bin

```

Step 2: We can use binwalk to study the binary closer: 
```
cincan run cincan/binwalk firmware.bin
```

This will output something like this:
```
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
53952         0xD2C0          U-Boot version string, "U-Boot 1.1.3 (Nov 19 2019 - 14:34:46)"
66560         0x10400         LZMA compressed data, properties: 0x5D, dictionary size: 8388608 bytes, uncompressed size: 2986732 bytes
1049088       0x100200        Squashfs filesystem, little endian, version 4.0, compression:xz, size: 2937112 bytes, 616 inodes, blocksize: 262144 bytes, created: 2019-11-19 07:18:30
```
Squashfs is a compressed Linux file system. Lets take a closer look what it contains.

Step 3: We can use dd to copy specific chunks of data from our drive. NOTE: There is a reason dd is jokingly called disk destroyer. Be careful and think before hitting enter!
```
dd if=firmware.bin of=filesystem skip=1049088 bs=1
```

This command will skip to the 1049088 byte and read blocks of 1 byte. The contents are strored into file called filesystem.

Step 4: Confirm that filesystem was copied correctly: 
```
file filesystem
```
This should output the following:
```
filesystem: Squashfs filesystem, little endian, version 4.0, 2937112 bytes, 616 inodes, blocksize: 262144 bytes, created: Tue Nov 19 07:18:30 2019
```
Looks good!

Step 5: Now we can decompress the file system: 
```
unsquash filesystem
```

This should produce a folder containing the file system of the router. Dig around and see what you can find!

BONUS: Check out passwd.bak file. Can you crack the Admin password?

