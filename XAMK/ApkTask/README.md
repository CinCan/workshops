# Apk analysis task
Android ransomware analysis

## Difficulty level

Beginner [x]

Intermediate [ ]

Professional [ ]


## Sample

ransomware.apk.zip present at this repository.

Password to the zip: infected

## Instructions

Recommended tools: cincan tools, jadx, dex2jar, your favorite code editor.

Short introduction to the tools used in this analysis: 

[Blogpost about dex2jar](https://cincan.io/blog/2019_12_09_dex2jar/)

[Blogpost about jadx](https://cincan.io/blog/2019_12_16_jadx/)

Step 0. Unzip the sample.

Step 1. Is the file malicious? Do previous analysis exist online?
Hint: Check if the hash of the .apk is found on some online malware database.

Step 2. Turn the APK file into .jar

Step 3. Use jadx to turn the .jar into java code.

Step 4. Analyze the code. Here are few things you might be interested to find out:

What files will the ransomware attempt to encrypt?
What method is used for encryption and how secure is it?
What's the command and control server the malware uses?
Does the malware collect information about the phone? If so, can you find the address it sends data to?
With this information could you decrypt the files or are you stuck paying the ransom?
What permissions the application requests?

## Bonus APK

Check out `anubis.apk.zip` in this directory same way as the previous APK. Password is the same old `infected`

What type of malware it is this time?

Try to answer previous questions too when analysing the APK. `grep` is your friend!
