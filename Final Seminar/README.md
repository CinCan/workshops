# Final Seminar 12.05.2020

Collection of CinCan Final Seminar presentations.

## Forensicating like a boss (JW)
https://gitlab.com/CinCan/workshops/-/blob/master/Final%20Seminar/CinCan_Minion__Forensicating_like_a_boss.pdf

## Teachers View (Tero Karvinen)
http://terokarvinen.com/slides/cincan-project-final-seminar-2020-05-12-karvinen-slides/#/title-slide

## Minion - Tool orchestration - Making and behind the scenes (Rauli Kaksonen)
https://gitlab.com/CinCan/workshops/-/blob/master/Final%20Seminar/Rauli_-_Minion_-_Final.pdf